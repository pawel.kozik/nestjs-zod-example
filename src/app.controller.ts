import {Body, Controller, Get, Post} from '@nestjs/common';
import { AppService } from './app.service';
import { PaymentAuthRequestDto } from "./payment-auth-request.dto";
import {PaymentAuthResponseDto} from "./payment-auth-response.dto";

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Post()
  authPayment(@Body() request: PaymentAuthRequestDto): PaymentAuthResponseDto {
    this.appService.validateAccount(request.accountId);

    this.appService.sendPartnerRequest(
      request.amount,
      request.creditorIban,
    )

    return {
      status: 'ACTC',
    };
  }
}
