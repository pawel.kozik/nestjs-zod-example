import {z} from "zod";
import {createZodDto} from "nestjs-zod";

const PaymentAuthRequestDtoSchema = z.object({
  accountId: z.string(),
  amount: z.number(),
  creditorIban: z.string(),
});

export class PaymentAuthRequestDto extends createZodDto(PaymentAuthRequestDtoSchema) {}