import {z} from "zod";
import {createZodDto} from "nestjs-zod";

const PaymentAuthResponseDtoSchema = z.object({
  status: z.string(),
});

export class PaymentAuthResponseDto extends createZodDto(PaymentAuthResponseDtoSchema) {}
